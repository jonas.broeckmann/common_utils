package net.novagamestudios.common_utils.compose

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.spring
import androidx.compose.foundation.ScrollState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.LayoutCoordinates
import androidx.compose.ui.layout.findRootCoordinates
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntRect
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.roundToIntRect
import androidx.compose.ui.unit.toOffset
import androidx.compose.ui.unit.toSize

abstract class ScrollAnchor<A> {
    private var scrollRootCoordinates by mutableStateOf<LayoutCoordinates?>(null)
    private var currentAnchorCoordinates by mutableStateOf<LayoutCoordinates?>(null)
    protected val anchor get() = currentAnchorCoordinates
    protected val scrollRoot get() = scrollRootCoordinates ?: currentAnchorCoordinates?.findRootCoordinates()

    protected abstract fun scrollRootOffset(alignment: A, scrollRootSize: IntSize): Int
    protected abstract fun anchorOffset(alignment: A, anchorSize: IntSize): Int
    protected abstract fun anchorPosition(anchorBounds: IntRect): Int

    suspend fun animateScrollToAnchor(
        scrollState: ScrollState,
        scrollRootAlignment: A,
        anchorAlignment: A = scrollRootAlignment,
        animationSpec: AnimationSpec<Float> = spring()
    ) {
        val scrollRoot = scrollRoot ?: return
        val anchor = anchor ?: return
        val anchorBounds = scrollRoot.localBoundingBoxOf(anchor, clipBounds = false).roundToIntRect()
        val offsetOfRoot = scrollRootOffset(scrollRootAlignment, scrollRoot.size)
        val anchorOffset = anchorOffset(anchorAlignment, anchorBounds.size)
        scrollState.animateScrollTo(anchorPosition(anchorBounds) + scrollState.value + anchorOffset - offsetOfRoot, animationSpec)
    }

    companion object {
        fun Modifier.scrollRootFor(anchor: ScrollAnchor<*>): Modifier {
            return onGloballyPositioned { anchor.scrollRootCoordinates = it }
//                .drawBehind {
//                    scrollState ?: return@drawBehind
//                    val scrollRoot = anchor.scrollRoot ?: return@drawBehind
//                    val a = anchor.anchor ?: return@drawBehind
//                    val anchorBounds = scrollRoot.localBoundingBoxOf(a)
//                    drawRect(Color.Magenta, anchorBounds.topLeft.copy(x = anchorBounds.topLeft.x + scrollState.value), anchorBounds.size)
//                }
        }
        @Suppress("MemberVisibilityCanBePrivate")
        fun Modifier.anchorFor(anchor: ScrollAnchor<*>): Modifier {
            return onGloballyPositioned { anchor.currentAnchorCoordinates = it }
        }
        fun Modifier.anchorForIf(anchor: ScrollAnchor<*>, predicate: () -> Boolean): Modifier {
            return if (predicate()) anchorFor(anchor) else this
        }
        fun Modifier.scrollRootDrawDebug(
            scrollState: ScrollState,
            it: ScrollAnchor<Alignment.Vertical>,
            scrollRootAlignment: Alignment.Vertical,
            anchorAlignment: Alignment.Vertical = scrollRootAlignment
        ) = drawBehind {
            // update if value changes
            scrollState.value
            val scrollRoot = it.scrollRoot ?: return@drawBehind
            val anchor = it.anchor ?: return@drawBehind
            val anchorBounds = scrollRoot.localBoundingBoxOf(anchor, clipBounds = false).roundToIntRect()
            val offsetOfRoot = it.scrollRootOffset(scrollRootAlignment, scrollRoot.size)
            val anchorOffset = it.anchorOffset(anchorAlignment, anchorBounds.size)
            val anchorPosition = it.anchorPosition(anchorBounds)

            // current anchor bounds
            drawRect(Color.Green, topLeft = IntOffset(anchorBounds.left, anchorPosition).toOffset(), size = anchorBounds.size.toSize())
            drawRect(Color.Yellow, topLeft = IntOffset(0, anchorPosition + anchorOffset).toOffset(), size = Size(size.width, 10f))
            // target position
            drawRect(Color.Red, topLeft = IntOffset(0, offsetOfRoot).toOffset(), size = Size(size.width, 10f))
        }
    }
}

class HorizontalScrollAnchor(
    private val layoutDirection: LayoutDirection = LayoutDirection.Ltr
) : ScrollAnchor<Alignment.Horizontal>() {
    override fun scrollRootOffset(alignment: Alignment.Horizontal, scrollRootSize: IntSize): Int {
        return alignment.align(0, scrollRootSize.width, layoutDirection)
    }
    override fun anchorOffset(alignment: Alignment.Horizontal, anchorSize: IntSize): Int {
        return alignment.align(0, anchorSize.width, layoutDirection)
    }
    override fun anchorPosition(anchorBounds: IntRect): Int {
        return when (layoutDirection) {
            LayoutDirection.Ltr -> anchorBounds.left
            LayoutDirection.Rtl -> anchorBounds.right
        }
    }
}

class VerticalScrollAnchor : ScrollAnchor<Alignment.Vertical>() {
    override fun scrollRootOffset(alignment: Alignment.Vertical, scrollRootSize: IntSize): Int {
        return alignment.align(0, scrollRootSize.height)
    }
    override fun anchorOffset(alignment: Alignment.Vertical, anchorSize: IntSize): Int {
        return alignment.align(0, anchorSize.height)
    }
    override fun anchorPosition(anchorBounds: IntRect): Int {
        return anchorBounds.top
    }
}