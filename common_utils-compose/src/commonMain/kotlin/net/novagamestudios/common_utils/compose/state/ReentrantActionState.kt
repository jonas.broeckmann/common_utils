package net.novagamestudios.common_utils.compose.state

import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class ReentrantActionState : State<Boolean> {

    override val value: Boolean get() = counter != 0
    private var counter by mutableStateOf(0)
    private val mutex = Mutex()

    private suspend fun suspendingAtomicIncrement() = mutex.withLock { ++counter }
    private suspend fun suspendingAtomicDecrement() = mutex.withLock { --counter }

    private fun atomicIncrement() = runBlocking { suspendingAtomicIncrement() }
    private fun atomicDecrement() = runBlocking { suspendingAtomicDecrement() }

    fun <R> trueWhileBlocking(block: () -> R): R {
        atomicIncrement()
        try {
            return block()
        } finally {
            atomicDecrement()
        }
    }

    suspend fun <R> trueWhile(block: suspend () -> R): R {
        suspendingAtomicIncrement()
        try {
            return block()
        } finally {
            suspendingAtomicDecrement()
        }
    }

    companion object {
        suspend fun <R> ReentrantActionState.trueWhileWithContext(context: CoroutineContext, block: suspend CoroutineScope.() -> R): R {
            suspendingAtomicIncrement()
            try {
                return withContext(context) {
                    block()
                }
            } finally {
                suspendingAtomicDecrement()
            }
        }
    }
}


