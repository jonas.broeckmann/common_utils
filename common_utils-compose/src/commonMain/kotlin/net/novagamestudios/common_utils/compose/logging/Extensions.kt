package net.novagamestudios.common_utils.compose.logging

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.NonSkippableComposable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.LoggerForFun
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.error
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.common_utils.core.logging.loggerLogger
import net.novagamestudios.common_utils.core.logging.verbose
import net.novagamestudios.common_utils.core.logging.warn
import net.novagamestudios.common_utils.core.logging.wtf


val LocalLogger = compositionLocalOf { loggerLogger }

@Suppress("NOTHING_TO_INLINE")
@Composable
inline fun ProvideLogger(
    logger: Logger = LoggerForFun(),
    noinline content: @Composable () -> Unit
) = CompositionLocalProvider(LocalLogger provides logger, content = content)

@NonSkippableComposable
@Composable
fun verbose(thr: Throwable? = null, msg: () -> Any?) = LocalLogger.current.verbose(thr, msg)
@NonSkippableComposable
@Composable
fun debug(thr: Throwable? = null, msg: () -> Any?) = LocalLogger.current.debug(thr, msg)
@NonSkippableComposable
@Composable
fun info(thr: Throwable? = null, msg: () -> Any?) = LocalLogger.current.info(thr, msg)
@NonSkippableComposable
@Composable
fun warn(thr: Throwable? = null, msg: () -> Any?) = LocalLogger.current.warn(thr, msg)
@NonSkippableComposable
@Composable
fun error(thr: Throwable? = null, msg: () -> Any?) = LocalLogger.current.error(thr, msg)
@NonSkippableComposable
@Composable
fun wtf(thr: Throwable? = null, msg: () -> Any?) = LocalLogger.current.wtf(thr, msg)


@Composable
fun rememberLogger(tag: String) = remember(tag) { Logger(tag) }