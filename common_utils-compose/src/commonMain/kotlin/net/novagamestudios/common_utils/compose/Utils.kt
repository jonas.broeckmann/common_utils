package net.novagamestudios.common_utils.compose

import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.GraphicsLayerScope
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.layout.ScaleFactor
import androidx.compose.ui.layout.times
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.toSize
import kotlin.math.roundToInt



operator fun IntSize.times(operand: Float) = toSize() * operand
operator fun IntSize.times(operand: ScaleFactor) = toSize() * operand
fun Size.round() = IntSize(width.roundToInt(), height.roundToInt())

var GraphicsLayerScope.scale: ScaleFactor
    get() = ScaleFactor(scaleX, scaleY)
    set(value) {
        scaleX = value.scaleX
        scaleY = value.scaleY
    }

val Placeable.size get() = IntSize(width, height)
