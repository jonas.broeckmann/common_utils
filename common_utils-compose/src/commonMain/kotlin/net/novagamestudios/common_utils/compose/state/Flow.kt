package net.novagamestudios.common_utils.compose.state

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow


fun <T> Flow<T>.collectAsStateIn(
    coroutineScope: CoroutineScope,
    initialValue: T
): State<T> = produceStateIn(coroutineScope, initialValue) { collect { value = it } }

fun <T> StateFlow<T>.collectAsStateIn(coroutineScope: CoroutineScope) = collectAsStateIn(coroutineScope, value)


@Composable
fun <T> rememberFlowAsState(
    initial: T,
    vararg keys: Any?,
    calculation: () -> Flow<T>
) = remember(keys, calculation).collectAsState(initial)

@Composable
fun <T> rememberStateFlowAsState(
    vararg keys: Any?,
    calculation: () -> StateFlow<T>
) = remember(keys, calculation).let { it.collectAsState(it.value) }



