package net.novagamestudios.common_utils.compose.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.ProgressIndicatorDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.unit.Dp

sealed interface Progress {
    data object Indeterminate : Progress
    @JvmInline
    value class Determinate(val value: Float) : Progress
    companion object
}

fun Progress(value: Float) = Progress.Determinate(value)
fun Progress(value: Float?) = if (value != null) Progress.Determinate(value) else Progress.Indeterminate

@Composable
fun LinearProgressIndicator(
    progress: Progress,
    modifier: Modifier = Modifier,
    color: Color = ProgressIndicatorDefaults.linearColor,
    trackColor: Color = ProgressIndicatorDefaults.linearTrackColor,
    strokeCap: StrokeCap = ProgressIndicatorDefaults.LinearStrokeCap
) = when (progress) {
    is Progress.Determinate -> androidx.compose.material3.LinearProgressIndicator(
        { progress.value },
        modifier,
        color,
        trackColor,
        strokeCap
    )
    is Progress.Indeterminate -> androidx.compose.material3.LinearProgressIndicator(
        modifier,
        color,
        trackColor,
        strokeCap
    )
}

@Composable
fun ProgressIndicatorDefaults.circularTrackColor(progress: Progress) = when (progress) {
    is Progress.Determinate -> circularDeterminateTrackColor
    is Progress.Indeterminate -> circularIndeterminateTrackColor
}

@Composable
fun CircularProgressIndicator(
    progress: Progress,
    modifier: Modifier = Modifier,
    color: Color = ProgressIndicatorDefaults.circularColor,
    strokeWidth: Dp = ProgressIndicatorDefaults.CircularStrokeWidth,
    trackColor: Color = ProgressIndicatorDefaults.circularTrackColor(progress),
    strokeCap: StrokeCap = ProgressIndicatorDefaults.CircularDeterminateStrokeCap
) = when (progress) {
    is Progress.Determinate -> androidx.compose.material3.CircularProgressIndicator(
        { progress.value },
        modifier,
        color,
        strokeWidth,
        trackColor,
        strokeCap
    )
    is Progress.Indeterminate -> androidx.compose.material3.CircularProgressIndicator(
        modifier,
        color,
        strokeWidth,
        trackColor,
        strokeCap
    )
}

@Composable
fun LinearLoadingBox(
    loading: Boolean,
    modifier: Modifier = Modifier,
    content: @Composable BoxScope.() -> Unit
) = LinearLoadingBox(Progress.Indeterminate.takeIf { loading }, modifier, content)

@Composable
fun LinearLoadingBox(
    progress: Progress?,
    modifier: Modifier = Modifier,
    content: @Composable BoxScope.() -> Unit
) {
    Box(modifier) {
        if (progress != null) {
            LinearProgressIndicator(
                progress,
                Modifier
                    .align(Alignment.Center)
                    .fillMaxWidth()
            )
        }
        Box(
            Modifier
                .wrapContentSize()
                .alpha(if (progress != null) 0f else 1f)
        ) {
            content()
        }
    }
}

@Composable
fun CircularLoadingBox(
    loading: Boolean,
    modifier: Modifier = Modifier,
    loadingContent: @Composable BoxScope.() -> Unit = { },
    content: @Composable BoxScope.() -> Unit
) = CircularLoadingBox(Progress.Indeterminate.takeIf { loading }, modifier, loadingContent, content)

@Composable
inline fun CircularLoadingBox(
    progress: Progress?,
    modifier: Modifier = Modifier,
    loadingContent: @Composable BoxScope.() -> Unit = { },
    content: @Composable BoxScope.() -> Unit
) {
    Box(modifier) {
        if (progress != null) {
            CircularProgressIndicator(progress, Modifier.align(Alignment.Center))
            loadingContent()
        }
        Box(
            Modifier
                .wrapContentSize()
                .alpha(if (progress != null) 0f else 1f)
        ) {
            content()
        }
    }
}