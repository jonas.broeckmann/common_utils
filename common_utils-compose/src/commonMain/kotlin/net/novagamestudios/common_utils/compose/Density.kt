package net.novagamestudios.common_utils.compose

import androidx.compose.animation.core.AnimationVector2D
import androidx.compose.animation.core.TwoWayConverter
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.calculateEndPadding
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp


operator fun Int.times(dp: Dp) = dp * this

@Composable
fun <T, R> T.withLocalDensity(block: Density.(T) -> R): R = with(LocalDensity.current) { block(this@withLocalDensity) }


val DpSize.Companion.VectorConverter get() = DpSizeToVector
private val DpSizeToVector = TwoWayConverter<DpSize, AnimationVector2D>(
    convertToVector = { AnimationVector2D(it.width.value, it.height.value) },
    convertFromVector = { DpSize(it.v1.dp, it.v2.dp) }
)

fun PaddingValues.calculatePadding(layoutDirection: LayoutDirection): Array<Dp> {
    val start = calculateStartPadding(layoutDirection)
    val top = calculateTopPadding()
    val end = calculateEndPadding(layoutDirection)
    val bottom = calculateBottomPadding()
    return arrayOf(start, top, end, bottom)
}