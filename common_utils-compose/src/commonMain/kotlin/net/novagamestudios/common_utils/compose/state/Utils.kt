package net.novagamestudios.common_utils.compose.state

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.AnimationVector
import androidx.compose.animation.core.TwoWayConverter
import androidx.compose.animation.core.animate
import androidx.compose.animation.core.spring
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.SnapshotMutationPolicy
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.structuralEqualityPolicy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

suspend fun <T, V : AnimationVector> MutableState<T>.animateTo(
    typeConverter: TwoWayConverter<T, V>,
    targetValue: T,
    initialVelocity: T? = null,
    animationSpec: AnimationSpec<T> = spring()
) = animate(typeConverter, value, targetValue, initialVelocity, animationSpec) { value, _ ->
    this.value = value
}

fun <T> latestNotNullPolicy(): SnapshotMutationPolicy<T> = object : SnapshotMutationPolicy<T> {
    override fun equivalent(a: T, b: T): Boolean = a == b
    override fun merge(previous: T, current: T, applied: T): T? = applied ?: current ?: previous
    override fun toString() = "latestNotNullPolicy"
}

@Composable
fun <T> rememberUpdatedNotNull(value: T?): State<T?> {
    val result = remember { mutableStateOf(value) }
    if (value != null) result.value = value
    return result
}


@Composable
fun <T> rememberMutableStateOf(
    value: T,
    policy: SnapshotMutationPolicy<T> = structuralEqualityPolicy()
) = remember { mutableStateOf(value, policy) }

@Composable
fun <T> rememberDerivedStateOf(
    vararg keys: Any?,
    calculation: () -> T
) = remember(*keys) { derivedStateOf(calculation) }
//@Composable
//fun <T> rememberDerivedStateOf(
//    vararg keys: Any?,
//    policy: SnapshotMutationPolicy<T>,
//    calculation: () -> T
//) = remember(*keys) { derivedStateOf(policy, calculation) }

inline fun <T> produceStateIn(
    coroutineScope: CoroutineScope,
    initialValue: T,
    crossinline producer: suspend MutableState<T>.() -> Unit
): State<T> {
    val state = mutableStateOf(initialValue)
    coroutineScope.launch { state.producer() }
    return state
}

