package net.novagamestudios.common_utils.compose.state

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.datastore.core.DataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


interface DataStoreState<T : Any> : State<T> {
    val values: StateFlow<T>
}

interface MutableDataStoreState<T : Any> : DataStoreState<T> {
    suspend fun loadInitial()
    fun provideInitial(value: T)

    suspend fun update(updater: suspend (T) -> T)
    fun tryUpdate(updater: suspend (T) -> T): Boolean
}

fun <T: Any> DataStore<T>.stateIn(
    coroutineScope: CoroutineScope,
    validator: T.(old: T?) -> T = { this }
): MutableDataStoreState<T> = MutableDataStoreStateImpl(this, coroutineScope, validator)

fun <T : Any> MutableDataStoreState<T>.loadInitialBlocking() = runBlocking { loadInitial() }
suspend fun <T : Any> MutableDataStoreState<T>.set(value: T) = update { value }
fun <T : Any> MutableDataStoreState<T>.trySet(value: T) = tryUpdate { value }


private class MutableDataStoreStateImpl<T : Any>(
    private val dataStore: DataStore<T>,
    private val coroutineScope: CoroutineScope,
    private val validator: T.(old: T?) -> T = { this }
) : MutableDataStoreState<T> {
    private lateinit var firstValue: T
    override val values: StateFlow<T> by lazy { dataStore.data.stateIn(coroutineScope, SharingStarted.Eagerly, firstValue) }
    private val state: State<T> by lazy { values.collectAsStateIn(coroutineScope) }
    override val value: T get() = state.value
    private val updaterChannel = Channel<suspend (T) -> T>(capacity = Channel.UNLIMITED)

    init {
        coroutineScope.launch {
            for (new in updaterChannel) dataStore.updateData { new(it).validator(it) }
        }
    }

    override fun provideInitial(value: T) {
        firstValue = value.validator(null)
    }
    override suspend fun loadInitial() = provideInitial(dataStore.data.first())

    override suspend fun update(updater: suspend (T) -> T) = updaterChannel.send(updater)
    override fun tryUpdate(updater: suspend (T) -> T) = updaterChannel.trySend(updater).isSuccess
}



class MockDataStore<T>(
    initial: T
) : DataStore<T> {
    override val data = MutableStateFlow(initial)
    override suspend fun updateData(transform: suspend (t: T) -> T): T {
        val new  = transform(data.value)
        data.value = new
        return new
    }
}

fun <T : Any> mockDataStoreState(
    initial: T,
    coroutineScope: CoroutineScope,
    validator: T.(old: T?) -> T = { this }
) = MockDataStore(initial).stateIn(coroutineScope, validator).also { it.provideInitial(initial) }

@Composable
fun <T : Any> rememberMockedDataStoreState(
    initial: T,
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    validator: T.(old: T?) -> T = { this }
) = remember { mockDataStoreState(initial, coroutineScope, validator) }


