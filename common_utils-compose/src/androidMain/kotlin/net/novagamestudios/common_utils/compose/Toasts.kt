package net.novagamestudios.common_utils.compose

import android.content.Context
import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext


class ToastsState {
    internal var builder by mutableStateOf<ToastBuilder?>(null)
    internal fun interface ToastBuilder : (Context) -> Toast

    private fun build(builder: ToastBuilder) {
        this.builder = builder
    }

    fun long(text: String, config: Toast.() -> Unit = { }) = build { context ->
        Toast.makeText(context, text, Toast.LENGTH_LONG).apply(config)
    }
    fun long(resId: Int, config: Toast.() -> Unit = { }) = build { context ->
        Toast.makeText(context, resId, Toast.LENGTH_LONG).apply(config)
    }
    fun short(text: String, config: Toast.() -> Unit = { }) = build { context ->
        Toast.makeText(context, text, Toast.LENGTH_SHORT).apply(config)
    }
    fun short(resId: Int, config: Toast.() -> Unit = { }) = build { context ->
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).apply(config)
    }
}


@Composable
fun Toasts(state: ToastsState) {
    state.builder?.let { builder ->
        builder(LocalContext.current).show()
        state.builder = null
    }
}
