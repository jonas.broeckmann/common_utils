package net.novagamestudios.common_utils.compose

import android.app.Application
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext


@Composable
inline fun <reified A : Application> application(): A = LocalContext.current.applicationContext as A
