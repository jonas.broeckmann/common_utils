package net.novagamestudios.common_utils.compose

import androidx.datastore.core.Serializer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import java.io.InputStream
import java.io.OutputStream


class JsonToDataStore<T>(
    override val defaultValue: T,
    private val serializer: KSerializer<T>,
    private val json: Json = Json
) : Serializer<T> {
    override suspend fun readFrom(input: InputStream): T {
        return json.decodeFromString(serializer, input.readBytes().decodeToString())
    }
    override suspend fun writeTo(t: T, output: OutputStream) = withContext(Dispatchers.IO) {
        output.write(json.encodeToString(serializer, t).encodeToByteArray())
    }
}
