package net.novagamestudios.common_utils.voyager.model

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import cafe.adriel.voyager.core.concurrent.ThreadSafeMap
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.rememberScreenModel
import cafe.adriel.voyager.core.platform.multiplatformName
import cafe.adriel.voyager.core.screen.Screen
import net.novagamestudios.common_utils.core.context


val globalScreenModels = ThreadSafeMap<String, ScreenModel>()

@Composable
inline fun <reified T : ScreenModel, R, A> rememberGlobalScreenModel(
    repositoryProvider: R,
    app: A,
    factory: GlobalScreenModelFactory<R, A, T>
): T = with(repositoryProvider) {
    val key = "${T::class.multiplatformName}:default"
    remember(key) { globalScreenModels.getOrPut(key) { factory.create(app) } as T }
}

context(S)
@Composable
inline fun <S : Screen, reified T : ScreenModel, R> rememberScreenModel(
    repositoryProvider: R,
    factory: ScreenModelFactory<R, S, T>
): T = with(repositoryProvider) {
    rememberScreenModel { factory.create(context<S>()) }
}
