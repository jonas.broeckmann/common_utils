package net.novagamestudios.common_utils.voyager.model

import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.screen.Screen


fun interface ScreenModelFactory<in R, in S : Screen, out T : ScreenModel> {
    context (R)
    fun create(screen: S): T
}

fun interface GlobalScreenModelFactory<in R, in A, out T : ScreenModel> {
    context (R)
    fun create(app: A): T
}


