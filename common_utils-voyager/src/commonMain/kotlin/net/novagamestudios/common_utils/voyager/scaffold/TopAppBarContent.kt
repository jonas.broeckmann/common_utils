package net.novagamestudios.common_utils.voyager.scaffold

import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarColors
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import cafe.adriel.voyager.navigator.Navigator
import net.novagamestudios.common_utils.voyager.AnimatedBackNavigationIcon
import net.novagamestudios.common_utils.voyager.BackNavigationHandler


interface TopAppBarContent : ContentWithBackNavigation, ScaffoldContent {
    @Composable
    fun TopAppBarNavigationIcon(navigator: Navigator): Unit = navigator.currentScaffoldContent()?.TopAppBarNavigationIcon(navigator) ?: Unit
    @Composable
    fun TopAppBarTitle(navigator: Navigator): Unit = navigator.currentScaffoldContent()?.TopAppBarTitle(navigator) ?: Unit
    @Composable
    fun TopAppBarActions(navigator: Navigator): Unit = navigator.currentScaffoldContent()?.TopAppBarActions(navigator) ?: Unit
}


@Composable
fun TopAppBar(
    content: TopAppBarContent,
    navigator: Navigator,
    modifier: Modifier = Modifier,
    title: @Composable () -> Unit = { content.TopAppBarTitle(navigator) },
    backNavigationIcon: @Composable (BackNavigationHandler) -> Unit = { AnimatedBackNavigationIcon(it) },
    navigationIcon: @Composable () -> Unit = { content.TopAppBarNavigationIcon(navigator) },
    actions: @Composable RowScope.() -> Unit = { content.TopAppBarActions(navigator) },
    expandedHeight: Dp = TopAppBarDefaults.TopAppBarExpandedHeight,
    windowInsets: WindowInsets = TopAppBarDefaults.windowInsets,
    colors: TopAppBarColors = TopAppBarDefaults.topAppBarColors(),
    scrollBehavior: TopAppBarScrollBehavior? = null
) = TopAppBar(
    title = title,
    modifier = modifier,
    navigationIcon = {
        backNavigationIcon(content.backNavigationHandler(navigator))
        navigationIcon()
    },
    actions = actions,
    expandedHeight = expandedHeight,
    windowInsets = windowInsets,
    colors = colors,
    scrollBehavior = scrollBehavior
)
