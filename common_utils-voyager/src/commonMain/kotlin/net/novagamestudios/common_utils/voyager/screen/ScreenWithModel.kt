package net.novagamestudios.common_utils.voyager.screen

import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.screen.Screen
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider


interface ScreenWithModel<out T : ScreenModel> : Screen, ScreenModelProvider<T>
