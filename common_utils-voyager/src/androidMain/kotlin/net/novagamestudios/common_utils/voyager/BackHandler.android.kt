package net.novagamestudios.common_utils.voyager

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable


@Composable
actual fun DefaultBackHandler(onBack: () -> Unit) = BackHandler(onBack = onBack)
