package net.novagamestudios.common_utils.core


fun <A, B> Pair<A?, B?>.takeIfNeitherNull(): Pair<A, B>? = first?.let { a -> second?.let { b -> a to b } }

fun <T> List<T?>.noNullsOrNull(): List<T>? {
    if (any { it == null }) return null
    @Suppress("UNCHECKED_CAST")
    return this as List<T>
}
