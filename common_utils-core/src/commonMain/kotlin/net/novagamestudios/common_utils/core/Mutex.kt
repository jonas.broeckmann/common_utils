package net.novagamestudios.common_utils.core

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext


suspend fun <T> Mutex.withReentrantLock(block: suspend () -> T): T {
    val key = ReentrantMutexContextKey(this)
    if (coroutineContext[key] != null) return block()
    return withContext(ReentrantMutexContextElement(key)) { withLock { block() } }
}

private class ReentrantMutexContextElement(override val key: ReentrantMutexContextKey) : CoroutineContext.Element
private data class ReentrantMutexContextKey(val mutex: Mutex) : CoroutineContext.Key<ReentrantMutexContextElement>


inline fun Mutex.tryWithLock(owner: Any? = null, block: () -> Unit): Boolean {
    if (!tryLock(owner)) return false
    try {
        block()
    } finally {
        unlock(owner)
    }
    return true
}
