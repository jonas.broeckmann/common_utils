package net.novagamestudios.common_utils.core.serialization

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

private data class Cursor(
    val data: String,
    private var i: Int
) {
    fun peek(): Char {
        if (!hasNext()) throw Exception("Unexpected end of data")
        return data[i]
    }
    fun next(): Char {
        val char = peek()
        i++
        return char
    }
    fun hasNext(): Boolean = i < data.length

    fun peek(n: Int): String? {
        if (i + n > data.length) return null
        return data.substring(i, i + n)
    }


    fun whitespace() {
        while (hasNext() && peek().isWhitespace()) next()
    }

    inline fun nextWhile(crossinline predicate: (Char) -> Boolean) = sequence {
        while (hasNext()) {
            if (!predicate(peek())) break
            yield(next())
        }
    }

    fun expect(expect: Char) {
        val actual = next()
        if (actual != expect) throw Exception("Expected '$expect' but got '$actual' at ${i - 1}")
    }
    fun expect(expect: Set<Char>): Char {
        val actual = next()
        if (actual !in expect) throw Exception(
            "Expected one of ${expect.joinToString(separator = ", ") { "'$it'" }} but got '$actual' at $i"
        )
        return actual
    }
    fun peekExpect(expect: Set<Char>): Char {
        val actual = peek()
        if (actual !in expect) throw Exception(
            "Expected one of ${expect.joinToString(separator = ", ") { "'$it'" }} but got '$actual' at $i"
        )
        return actual
    }

    fun tryNext(str: String): Boolean {
        if (i + str.length > data.length) return false
        if (data.substring(i, i + str.length) != str) return false
        i += str.length
        return true
    }

    override fun toString(): String {
        val window = 20
        return data.substring((i - window / 2)..<i) + "|" + data.substring(i..<(i + window / 2))
    }
}

private fun Cursor.parseJSArray(): JsonArray {
    whitespace()
    expect('[')
    whitespace()
    if (peek() == ']') return JsonArray(emptyList())
    val content = mutableListOf<JsonElement>()
    while (true) {
        content += parseJSElement()
        whitespace()
        if (expect(setOf(',', ']')) == ']') break
    }
    return JsonArray(content)
}

private fun Cursor.parseJSObject(): JsonObject {
    whitespace()
    expect('{')
    whitespace()
    if (peek() == '}') return JsonObject(emptyMap())
    val content = mutableMapOf<String, JsonElement>()
    while (true) {
        whitespace()
        val key = tryParseJSIdentifier()
            ?: tryParseJSString()
            ?: tryParseJSNumber()?.toString()
            ?: throw Exception("Invalid key starting with '${peek()}'")
        if (key in content) throw Exception("Duplicate key \"$key\"")
        whitespace()
        expect(':')
        whitespace()
        content[key] = parseJSElement()
        whitespace()
        if (expect(setOf(',', '}')) == '}') break
    }
    return JsonObject(content)
}


private fun Cursor.tryParseJSBoolean(): Boolean? {
    if (tryNext("true")) return true
    if (tryNext("false")) return false
    return null
}


private fun Cursor.tryParseJSIdentifier(): String? {
    if (peek().let { it.isLetter() || it == '$' || it == '_' }) {
        return nextWhile { it.isLetterOrDigit() || it == '$' || it == '_' }.joinToString("")
    }
    return null
}
private fun Cursor.tryParseJSNumber(): Number? {
    if (peek().isDigit()) {
        return nextWhile { it.isDigit() }.fold(0) { number, char -> number * 10 + char.digitToInt() }
    }
    return null
}

private fun Cursor.parseJSStringChar(start: Char): Char? {
    val char = next()
    if (char == '\\') {
        return when (val esc = next()) {
            '0' -> Char(0)
            'b' -> '\b'
            'f' -> TODO()
            'n' -> '\n'
            'r' -> '\r'
            't' -> '\t'
            'v' -> TODO()
            '\'' -> '\''
            '"' -> '"'
            '\\' -> '\\'
            'x' -> Char("${next()}${next()}".toInt(16))
            'u' -> Char("${next()}${next()}${next()}${next()}".toInt(16))
            else -> throw Exception("Unknown escaped char '$esc'")
        }
    }
    if (char == start) return null
    return char
}

private fun Cursor.tryParseJSString(): String? {
    if (peek().let { it == '\'' || it == '"' }) {
        val start = next()
        var str = ""
        while (true) {
            val char = parseJSStringChar(start) ?: break
            str += char
        }
        return str
    }
    return null
}

@OptIn(ExperimentalSerializationApi::class)
private fun Cursor.parseJSPrimitive(): JsonPrimitive {
    whitespace()
    if (tryNext("null")) return JsonPrimitive(null)
    tryParseJSBoolean()?.let { return JsonPrimitive(it) }
    tryParseJSNumber()?.let { return JsonPrimitive(it) }
    tryParseJSString()?.let { return JsonPrimitive(it) }

    tryParseJSIdentifier()?.let { return JsonPrimitive("$$it") }
    throw Exception("Unexpected character '${peek()}'")
}

private fun Cursor.parseJSElement(): JsonElement {
    whitespace()
    return when (peek()) {
        '{' -> parseJSObject()
        '[' -> parseJSArray()
        else -> parseJSPrimitive()
    }
}

object JSParser {
    fun parseToJson(str: String): JsonElement {
        Cursor(str, 0).run {
            return parseJSElement().also {
                whitespace()
                if (hasNext()) throw Exception("Expected end of data")
            }
        }
    }
}