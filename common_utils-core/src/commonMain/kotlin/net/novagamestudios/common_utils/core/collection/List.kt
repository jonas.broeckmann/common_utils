package net.novagamestudios.common_utils.core.collection

fun <T> MutableList<T>.pad(size: Int, padding: (Int) -> T) {
    while (this.size != size) add(padding(this.size))
}

fun <T> MutableList<T>.setOrAddOrFill(i: Int, value: T, filler: (Int) -> T) {
    if (size < i) {
        pad(i - 1, filler)
        add(value)
    } else if (size == i) {
        add(value)
    } else {
        set(i, value)
    }
}

fun <T> MutableList<T>.setOrAdd(i: Int, value: T) {
    setOrAddOrFill(i, value) { throw IndexOutOfBoundsException() }
}

fun <T> MutableList<T>.setAll(values: Iterable<T>) {
    clear()
    addAll(values)
}

fun <T> List<T>.indexOfFirstOrFirstIfEmpty(predicate: (T) -> Boolean) = if (isEmpty()) 0 else indexOfFirst(predicate)