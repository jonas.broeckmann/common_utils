package net.novagamestudios.common_utils.core.logging

import net.novagamestudios.common_utils.core.InternalCommonUtilsApi

/**
 * Interface for the Logger.
 * Normally you should pass the logger tag to logging methods.
 * This can be inconvenient because you should store the tag somewhere or hardcode it,
 *   which is considered to be a bad practice.
 *
 * Instead of hardcoding tags, it provides an [Logger] interface. You can just add the interface to
 *   any of your classes, and use any of the provided extension functions, such as
 *   [Logger.debug] or [Logger.error].
 *
 * The tag is the simple class name by default, but you can change it to anything you want just
 *   by overriding the [loggerTag] property.
 */
interface Logger {
    /**
     * The logger tag used in extension functions for the [Logger].
     * Note that the tag length should not be more than 23 symbols.
     */
    val loggerTag: String get() = javaClass.simpleName.asTag

    enum class Level {
        Verbose, Debug, Info, Warning, Error, WTF
    }
}

const val checkLogLevel = false

@InternalCommonUtilsApi
val loggerLogger = Logger<Logger>()

inline fun <reified T: Any> Logger(): Logger = Logger(T::class.java)
fun Logger(clazz: Class<*>): Logger = Logger(clazz.simpleName.asTag)
fun Logger(tag: String): Logger {
    if (tag.length > 23) loggerLogger.warn { "The maximum tag length is 23, got $tag" }
    return object : Logger {
        override val loggerTag: String get() = tag
    }
}

@Suppress("FunctionName", "NOTHING_TO_INLINE")
inline fun LoggerForFun(level: Int = 0): Logger = Logger(currentMethodName(level))

@Suppress("NOTHING_TO_INLINE")
inline fun currentMethodName(level: Int = 0): String = Thread.currentThread().stackTrace[2 + level].methodName


/**
 * Send a log message with the [Logger.Level.Verbose] severity.
 * Note that the log message will not be written if the current log level is above [Logger.Level.Verbose].
 * The default log level is [Logger.Level.Info].
 *
 * @param msg the message text to log. `null` value will be represent as "null", for any other value
 *   the [Any.toString] will be invoked.
 * @param thr an exception to log (optional).
 */
fun Logger.verbose(thr: Throwable? = null, msg: () -> Any?) = log(msg, thr, Logger.Level.Verbose)

/**
 * Send a log message with the [Logger.Level.Debug] severity.
 * Note that the log message will not be written if the current log level is above [Logger.Level.Debug].
 * The default log level is [Logger.Level.Info].
 *
 * @param msg the message text to log. `null` value will be represent as "null", for any other value
 *   the [Any.toString] will be invoked.
 * @param thr an exception to log (optional).
 */
fun Logger.debug(thr: Throwable? = null, msg: () -> Any?) = log(msg, thr, Logger.Level.Debug)

/**
 * Send a log message with the [Logger.Level.Info] severity.
 * Note that the log message will not be written if the current log level is above [Logger.Level.Info]
 *   (it is the default level).
 *
 * @param msg the message text to log. `null` value will be represent as "null", for any other value
 *   the [Any.toString] will be invoked.
 * @param thr an exception to log (optional).
 */
fun Logger.info(thr: Throwable? = null, msg: () -> Any?) = log(msg, thr, Logger.Level.Info)

/**
 * Send a log message with the [Logger.Level.Warning] severity.
 * Note that the log message will not be written if the current log level is above [Logger.Level.Warning].
 * The default log level is [Logger.Level.Info].
 *
 * @param msg the message text to log. `null` value will be represent as "null", for any other value
 *   the [Any.toString] will be invoked.
 * @param thr an exception to log (optional).
 */
fun Logger.warn(thr: Throwable? = null, msg: () -> Any?) = log(msg, thr, Logger.Level.Warning)

/**
 * Send a log message with the [Logger.Level.Error] severity.
 * Note that the log message will not be written if the current log level is above [Logger.Level.Error].
 * The default log level is [Logger.Level.Info].
 *
 * @param msg the message text to log. `null` value will be represent as "null", for any other value
 *   the [Any.toString] will be invoked.
 * @param thr an exception to log (optional).
 */
fun Logger.error(thr: Throwable? = null, msg: () -> Any?) = log(msg, thr, Logger.Level.Error)

/**
 * Send a log message with the "What a Terrible Failure" severity.
 * Report an exception that should never happen.
 *
 * @param msg the message text to log. `null` value will be represent as "null", for any other value
 *   the [Any.toString] will be invoked.
 * @param thr an exception to log (optional).
 */
fun Logger.wtf(thr: Throwable? = null, msg: () -> Any?) = log(msg, thr, Logger.Level.WTF)


internal expect inline fun Logger.log(
    message: () -> Any?,
    thr: Throwable?,
    level: Logger.Level
)

private val String.asTag get() = take(23)



val NoOpLogger = Logger("NoOpLogger")

