package net.novagamestudios.common_utils.core


@Target(
    allowedTargets = [
        AnnotationTarget.CLASS,
        AnnotationTarget.PROPERTY,
        AnnotationTarget.FUNCTION,
        AnnotationTarget.TYPEALIAS,
        AnnotationTarget.CONSTRUCTOR,
        AnnotationTarget.FIELD
    ]
)
@RequiresOptIn(
    level = RequiresOptIn.Level.ERROR,
    message = "This API is Common Utils Internal. It may be changed in the future without notice."
)
annotation class InternalCommonUtilsApi
