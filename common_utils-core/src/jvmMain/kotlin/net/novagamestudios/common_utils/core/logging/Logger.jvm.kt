package net.novagamestudios.common_utils.core.logging

var LoggerLevel = Logger.Level.Verbose

internal actual inline fun Logger.log(
    message: () -> Any?,
    thr: Throwable?,
    level: Logger.Level
) {
    if (this === NoOpLogger) return
    val tag = loggerTag
    if (!checkLogLevel || level == Logger.Level.WTF || level.isLoggable(tag)) {
        val msg = message().toString()
        if (thr == null) {
            println("$tag: $msg")
        } else {
            println("$tag: $msg")
            thr.printStackTrace()
        }
    }
}

internal fun Logger.Level.isLoggable(tag: String) = this >= LoggerLevel
