package net.novagamestudios.common_utils.core.logging

import android.util.Log

internal actual inline fun Logger.log(
    message: () -> Any?,
    thr: Throwable?,
    level: Logger.Level
) {
    if (this === NoOpLogger) return
    val tag = loggerTag
    if (!checkLogLevel || level == Logger.Level.WTF || level.isLoggable(tag)) {
        val msg = message().toString()
        if (thr == null) when (level) {
            Logger.Level.Verbose -> Log.v(tag, msg)
            Logger.Level.Debug -> Log.d(tag, msg)
            Logger.Level.Info -> Log.i(tag, msg)
            Logger.Level.Warning -> Log.w(tag, msg)
            Logger.Level.Error -> Log.e(tag, msg)
            Logger.Level.WTF -> Log.wtf(tag, msg)
        } else when (level) {
            Logger.Level.Verbose -> Log.v(tag, msg, thr)
            Logger.Level.Debug -> Log.d(tag, msg, thr)
            Logger.Level.Info -> Log.i(tag, msg, thr)
            Logger.Level.Warning -> Log.w(tag, msg, thr)
            Logger.Level.Error -> Log.e(tag, msg, thr)
            Logger.Level.WTF -> Log.wtf(tag, msg, thr)
        }
    }
}

internal fun Logger.Level.isLoggable(tag: String) = Log.isLoggable(tag, when (this) {
    Logger.Level.Verbose -> Log.VERBOSE
    Logger.Level.Debug -> Log.DEBUG
    Logger.Level.Info -> Log.INFO
    Logger.Level.Warning -> Log.WARN
    Logger.Level.Error -> Log.ERROR
    Logger.Level.WTF -> Log.ASSERT
})
