enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

rootProject.name = "common_utils"
include(":common_utils-core")
include(":common_utils-compose")
include(":common_utils-voyager")
